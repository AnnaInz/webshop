<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 10.05.2017
  Time: 16:01
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<spr:url value="/" var="homeUrl"/>
<a href="${homeUrl}"> Стартовая страница </a>
|
<c:if test="${userJSP.ID == 0 || not empty logoutMessage}">
    <spr:url value="/login" var="loginUrl"/>
    <a href="${loginUrl}"> Залогиниться </a>
    |
    <spr:url value="/registration" var="registerUrl"/>
    <a href="${registerUrl}"> Регистрация </a>
</c:if>




<c:if test="${userJSP.ID != 0 && empty logoutMessage}">


    <c:if test='${userJSP.status.equals("администратор")}'>
        <spr:url value="/admin" var="adminUrl"/>
        <a href="${adminUrl}"> Мои товары </a>
        |
    </c:if>

    <spr:url value="/logout" var="logoutUrl"/>
    <a href="${logoutUrl}"> Вылогиниться </a>
    |


    <spr:url value="/cart" var="productListUrl"/>
    <a href="${productListUrl}"> Корзина </a>
    <br><br>

</c:if>

<hr>
${userJSP.name} ${userJSP.surname}
<hr>

