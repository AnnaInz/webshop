<%--
  Created by IntelliJ IDEA.
  User: РђРЅРЅР°
  Date: 13.05.2017
  Time: 21:27
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>



<ul>

    <li> ${lc1.name} </li>

    <ul>
        <c:if test="${not empty lc1.subcategory}">
            <c:forEach var="s" items="${lc1.subcategory}">
                <spr:url value="/category/{slug}" var="articleUrl">
                    <spr:param name="slug" value="${s.name}" />
                </spr:url>

                <li> <a href="${articleUrl}">  ${s.name} </a> </li>
            </c:forEach>
        </c:if>
    </ul>

    <li> ${lc2.name} </li>

    <ul>
        <c:if test="${not empty lc2.subcategory}">
            <c:forEach var="s" items="${lc2.subcategory}">
                <spr:url value="/category/{slug}" var="articleUrl">
                    <spr:param name="slug" value="${s.name}" />
                </spr:url>

                <li> <a href="${articleUrl}">  ${s.name} </a> </li>
            </c:forEach>
        </c:if>
    </ul>
</ul>
