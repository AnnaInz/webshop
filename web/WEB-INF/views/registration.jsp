<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 13.05.2017
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test="${successfulRegistration}">
    ${messageSuccessfulRegistration}
</c:if>


<c:if test='${userJSP.ID == 0}'>
    <c:if test="${!successfulRegistration}">
        <span style="color: #d18cff; ">${messageUnsuccessfulRegistration}</span>

        <spring:form method="post" action="registration" modelAttribute="registeringUserJSP">

            <spring:label path="email">Электронная почта</spring:label><br>
            <spring:input path="email" /><br>

            <spring:label path="password">Пароль</spring:label><br>
            <spring:password path="password" /><br>

            <spring:label path="surname">Фамилия</spring:label><br>
            <spring:input path="surname" /><br>

            <spring:label path="name">Имя</spring:label><br>
            <spring:input path="name" /><br>

            <spring:label path="secondName">Отчество</spring:label><br>
            <spring:input path="secondName" /><br>

            <spring:label path="address">Адрес</spring:label><br>
            <spring:input size="50px" path="address" /><br>

            <input type="submit" value="Зарегистрироваться" />

        </spring:form>
    </c:if>
</c:if>


</body>
</html>
