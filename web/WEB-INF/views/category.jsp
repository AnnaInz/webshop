<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 13.05.2017
  Time: 21:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>



<%@ include file="header.jsp" %>
<%@ include file="menu.jsp" %>

${categoryJSP}

<br/>

<c:forEach var="product" items="${productJSP}">
    <c:forEach var="img" items="${product.photos}">
        <img src="<c:url value="${img}"/>" width="400px" height="250px">
        <br/>
    </c:forEach>
    ${product}

    <c:if test="${userJSP.ID != 0}">
        <br/>
        <spring:form action="${categoryJSP}" method="post">
            <input type="hidden" name="product_id" value="${product.serialNumber}">
            <input type="submit" value="Добавить в корзину"/>
        </spring:form>
    </c:if>
    <br/>
    <br/>
</c:forEach>


</body>
</html>
