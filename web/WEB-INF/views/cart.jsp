<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 14.05.2017
  Time: 14:08
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Корзина</title>
</head>>

<%@ include file="header.jsp" %>

<br/>

<h3>Корзина</h3>

<c:if test="${userJSP.ID== 0}">
    Вы не авторизованны!
</c:if>

<c:if test="${empty productListJSP && userJSP.ID != 0}">
    Корзина пуста
</c:if>

<c:if test="${not empty productListJSP && userJSP.ID != 0}">
    <h1>${message}</h1>
    <br>
    Общая стоимость: ${priceJSP}
    <br/>
    <br/>
    <c:forEach var="product" items="${productListJSP}">

        ${product.element0}
        <br/><br/>
        Количество: ${product.element1}
        <br/>
        <c:if test="${userJSP.ID != 0}">
            <br/>
            <spring:form action="cart" method="post">
                <input type="hidden" name="status_order" value="delete"/>
                <input type="hidden" name="product_id" value="${product.element0.serialNumber}"/>
                <input type="submit" value="Удалить из корзины"/>
            </spring:form>
        </c:if>
        <br/>
        <br/>
    </c:forEach>

    <spring:form action="cart" method="post">
        <input type="hidden" name="status_order" value="ordering"/>
        <input type="submit" value="Оформить заказ"/>
    </spring:form>
</c:if>


</body>
</html>
