<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 13.05.2017
  Time: 16:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>


<%@ include file="header.jsp" %>

<c:if test='${userJSP.status.equals("администратор")}'>

    <spring:form method="get" action="new_product">
        <input type="submit" value="Добавить товар" />
    </spring:form>

    <c:forEach var="product" items="${productsJSP}">
        <c:forEach var="img" items="${product.photos}">
            <img src="<c:url value="${img}"/>" width="200px" height="125px">
            <br/>
        </c:forEach>
        ${product}<br/>
        <spring:form action="admin" method="post">
            <input type="hidden" name="st" value="del" />
            <input type="hidden" name="serial_number" value="${product.serialNumber}"/>
            <input type="submit" value="Удалить товар"/>
        </spring:form>
        <br/>
        <br/>
    </c:forEach>
</c:if>


</body>
</html>
