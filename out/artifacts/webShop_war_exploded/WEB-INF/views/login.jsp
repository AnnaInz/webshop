<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 10.05.2017
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<%@ include file="header.jsp" %>

<c:if test="${userJSP.ID == 0}">
    ${wrongPasswordorLogin}
    <spring:form action="login" method="post" modelAttribute="predUser">
        <spring:label path="email"> email </spring:label><br>
        <spring:input path="email"/><br>
        <spring:label path="password"> password </spring:label><br>
        <spring:input path="password"/><br>

        <input type="submit" value="Войти"/>
    </spring:form>

</c:if>
<c:if test="${userJSP.ID != 0}"> </c:if>

</body>
</html>
