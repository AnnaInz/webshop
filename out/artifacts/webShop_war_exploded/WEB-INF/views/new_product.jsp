<%--
  Created by IntelliJ IDEA.
  User: Анна
  Date: 13.05.2017
  Time: 19:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New Product</title>
</head>
<body>

<%@ include file="header.jsp" %>

<c:if test='${userJSP.status.equals("администратор")}'>

    <spring:form id="newProductForm" method="post" action="new_product">

        <label>Название</label><br>
        <input type="text" name="product_name"/><br>

        <label>Цена</label><br>
        <input type="number" name="price" min="1" max="10000000" step="0.50"/><br>

        <label>Описание</label><br>
        <input type="text" name="discription"/><br>

        <label>Скидка</label><br>
        <input type="number" name="discount" min="0" max="100" step="1"/><br>

        <label>Категория</label><br>
        <select name="category">
            <c:forEach var="category" items="${llcc}">
                <c:if test="${empty category.subcategory}">
                    <option value="${category.id}" >${category.name}</option>
                </c:if>
            </c:forEach>
        </select> <br>
        <input type="submit" value="Добавить" />

    </spring:form>
</c:if>

</body>
</html>
