package ru.webShop.models;

import java.util.List;

/**
 * Created by Анна on 04.05.2017.
 */
public class Category {
    private int parentCategory;
    private String name;
    private List<Category> subcategory;
    private int id;

    public int getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(int parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(List<Category> subcategory) {
        this.subcategory = subcategory;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Category{" +
                "parentCategory=" + parentCategory +
                ", name='" + name + '\'' +
                ", subcategory=" + subcategory  +
                ", id=" + id +
                '}';
    }
}
