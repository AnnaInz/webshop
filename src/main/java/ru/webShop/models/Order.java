package ru.webShop.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Анна on 04.05.2017.
 */
public class Order {
    private long ID;
    private User user;
    private List<Pair<Product, Integer>> list; //список продуктов и количество каждого <серийный номер, количество>
    private String status;
    private double price;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Pair<Product, Integer>> getList() {
        return list;
    }

    public void setList(List<Pair<Product, Integer>> list) {
        this.list = list;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order{" +
                "ID=" + ID +
                ", user=" + user +
                ", list=" + list +
                ", status='" + status + '\'' +
                ", price=" + price +
                '}';
    }
}
