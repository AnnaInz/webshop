package ru.webShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.webShop.dao.impl.CategoryDaoImpl;
import ru.webShop.dao.impl.OrderDaoImpl;
import ru.webShop.dao.impl.ProductDaoImpl;
import ru.webShop.dao.impl.UserDaoImpl;
import ru.webShop.models.Pair;
import ru.webShop.models.Product;
import ru.webShop.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;

/**
 * Created by Анна on 10.05.2017.
 */
@Controller
@SessionAttributes("userJSP")
public class OrderController {

    @Autowired
    private UserDaoImpl userDao;

    @Autowired
    private ProductDaoImpl productDao;

    @Autowired
    private CategoryDaoImpl categoryDao;

    @Autowired
    private OrderDaoImpl orderDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public ModelAndView getProductList(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("cart");

        List<Pair<Product, Integer>> productsInCart = productDao.getByUser((int) user.getID());

        double price = 0.0;
        for (Pair<Product, Integer> product: productsInCart)
            price += (product.getElement0().getPrice() - product.getElement0().getPrice() * product.getElement0().getDiscount() / 100) * product.getElement1();


        modelAndView.addObject("priceJSP", price);
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("productListJSP", productsInCart );

        return modelAndView;
    }

    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    public ModelAndView executeProductList(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("cart");

        String status = request.getParameter("status_order");

        if (status.equals("delete")) {
            String productId = request.getParameter("product_id");
            orderDao.deteteFromCart(user, productDao.getProductBySerialNumber(Integer.valueOf(productId)), 1);
        }

        List<Pair<Product, Integer>> productsInCart = productDao.getByUser((int)user.getID());

        double price = 0.0;
        for (Pair<Product, Integer> product: productsInCart) {
            price += (product.getElement0().getPrice() - product.getElement0().getPrice() * product.getElement0().getDiscount() / 100) * product.getElement1();
        }

        System.out.println( price);
        modelAndView.addObject("priceJSP", price);
        modelAndView.addObject("userJSP", user);
        modelAndView.addObject("productListJSP", productsInCart);

        if (status.equals("ordering")){
            orderDao.ordering(user, price);
            modelAndView.addObject("message", "Вы подтвердили заказ. Заказ оформляется.");
        }

        return modelAndView;
    }
}
