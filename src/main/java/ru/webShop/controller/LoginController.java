package ru.webShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.webShop.dao.impl.CategoryDaoImpl;
import ru.webShop.dao.impl.UserDaoImpl;
import ru.webShop.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * Created by Анна on 10.05.2017.
 */

@Controller
@SessionAttributes("userJSP")
public class LoginController {
    @Autowired
    private UserDaoImpl userDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public ModelAndView displayLogin(HttpServletRequest request, HttpServletResponse response, SessionStatus sessionStatus, @ModelAttribute("userJSP") User user) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");

        User user_ = new User();
        modelAndView.addObject("predUser", user_);
        modelAndView.addObject("userJSP", user);

        return modelAndView;
    }

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ModelAndView correctLogin(HttpServletRequest request, HttpServletResponse response, SessionStatus sessionStatus, @ModelAttribute("userJSP") User user, @ModelAttribute("predUser") User user_) throws SQLException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");

        if (userDao.isValidUser(user_.getEmail(), user_.getPassword())){
            user = userDao.getUserByEmail(user_.getEmail());
        }
        else {
            modelAndView.addObject("wrongPasswordorLogin", "Неверный логин или пароль");
        }
        modelAndView.addObject("userJSP", user);
        return modelAndView;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView displayLogout(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model = new ModelAndView("index");
        model.addObject("userJSP", user);
        request.setAttribute("logoutMessage", "Вы вышли");

        sessionStatus.setComplete();

        return model;
    }
}
