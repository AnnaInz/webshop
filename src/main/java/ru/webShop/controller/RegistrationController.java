package ru.webShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.webShop.dao.impl.CategoryDaoImpl;
import ru.webShop.dao.impl.UserDaoImpl;
import ru.webShop.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Анна on 10.05.2017.
 */

@Controller
@SessionAttributes("userJSP")
public class RegistrationController {

    @Autowired
    private UserDaoImpl userDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/registration",method = RequestMethod.GET)
    public ModelAndView displayRegister(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus)
    {
        ModelAndView model = new ModelAndView("registration");
        User registeringUser = new User();
        model.addObject("registeringUserJSP", registeringUser);

        model.addObject("userJSP", user);
        return model;
    }


    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView useRegister(@ModelAttribute("userJSP") User user, @ModelAttribute("registeringUserJSP")User registeringUser, HttpServletRequest request, HttpServletResponse response, SessionStatus sessionStatus)
    {
        ModelAndView model= null;
        try
        {
            boolean notExistUserInDB = !userDao.existUser(registeringUser.getEmail());

            Pattern pattern = Pattern.compile("([A-Za-z0-9]{1,}[\\\\-]{0,1}[A-Za-z0-9]{1,}[\\\\.]{0,1}[A-Za-z0-9]{1,})+@([A-Za-z0-9]{1,}[\\\\-]{0,1}[A-Za-z0-9]{1,}[\\\\.]{0,1}[A-Za-z0-9]{1,})+[\\\\.]{1}[a-z]{2,4}");
            Matcher matcher = pattern.matcher(registeringUser.getEmail());
            System.out.println(matcher.matches());
            boolean emailIsEmail = matcher.matches();

            if(notExistUserInDB && emailIsEmail) {
                boolean isValidPassword = registeringUser.getPassword().length() >= 3;
                if(isValidPassword) {
                    registeringUser.setStatus("пользователь");
                    String str = userDao.add(registeringUser);

                    user = userDao.getUserByEmail(registeringUser.getEmail());

                    request.setAttribute("registeringUserJSP", registeringUser);
                    model = new ModelAndView("registration");

                    model.addObject("userJSP", user);
                    model.addObject("successfulRegistration", true);

                    request.setAttribute("messageSuccessfulRegistration", "Регистрация успешно пройдена");
                }
                else
                {
                    model = new ModelAndView("registration");
                    model.addObject("registeringUserJSP", registeringUser);
                    model.addObject("userJSP", user);
                    model.addObject("successfulRegistration", false);
                    request.setAttribute("messageUnsuccessfulRegistration", "Пароль должен быть длиной >= 3");
                }
            }
            else
            {
                model = new ModelAndView("registration");
                model.addObject("registeringUserJSP", registeringUser);
                model.addObject("userJSP", user);

                model.addObject("successfulRegistration", false);
                 if (!notExistUserInDB)
                    request.setAttribute("messageUnsuccessfulRegistration", "Пользователь с таким логином уже есть в системе.");
                 else
                     request.setAttribute("messageUnsuccessfulRegistration", "Введённый Вами логин не является почтовым адресом.");

            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return model;
    }

}
