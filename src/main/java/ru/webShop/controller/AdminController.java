package ru.webShop.controller;

import org.springframework.beans.CachedIntrospectionResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.webShop.dao.impl.CategoryDaoImpl;
import ru.webShop.dao.impl.ProductDaoImpl;
import ru.webShop.dao.impl.UserDaoImpl;
import ru.webShop.models.Category;
import ru.webShop.models.Product;
import ru.webShop.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Анна on 10.05.2017.
 */

@Controller
@SessionAttributes("userJSP")
public class AdminController {

    @Autowired
    private CategoryDaoImpl categoryDao;

    @Autowired
    private ProductDaoImpl productDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView getAdminPage(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("admin");
        modelAndView.addObject("userJSP", user);

        modelAndView.addObject("lc1", categoryDao.get(1));
        modelAndView.addObject("lc2", categoryDao.get(2));

        modelAndView.addObject("productsJSP", productDao.getByManufacturer(user.getName()));

        return modelAndView;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public ModelAndView useAdminPage(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("lc1", categoryDao.get(1));
        modelAndView.addObject("lc2", categoryDao.get(2));

        modelAndView.setViewName("admin");

        String status = request.getParameter("st");
        System.out.println("Status = " + status);

        if (status.equals("del")) {
            String productId = request.getParameter("serial_number");
            productDao.delete(Integer.valueOf(productId));
        }

        modelAndView.addObject("userJSP", user);

        modelAndView.addObject("productsJSP", productDao.getByManufacturer(user.getName()));

        return modelAndView;
    }


    @RequestMapping(value = "/new_product", method = RequestMethod.GET)
    public ModelAndView newProducr(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        String manufacturer = user.getName();

        if (manufacturer.equals("Анна")) {
            modelAndView.addObject("lc", categoryDao.get(1));
            modelAndView.addObject("llcc", categoryDao.list(1));
        }

        if (manufacturer.equals("Атонина")) {
            modelAndView.addObject("lc", categoryDao.get(2));
            modelAndView.addObject("llcc", categoryDao.list(2));
        }

        modelAndView.addObject("userJSP", user);

        return modelAndView;
    }

    @RequestMapping(value = "/new_product", method = RequestMethod.POST)
    public ModelAndView addNewProduct(HttpServletRequest request, HttpServletResponse response, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("new_product");

        String manufacturer = user.getName();

        if (manufacturer.equals("Анна") && user.getStatus().equals("администратор")) {
            modelAndView.addObject("lc", categoryDao.get(1));
            modelAndView.addObject("llcc", categoryDao.list(1));
        }

        if (manufacturer.equals("Атонина")&& user.getStatus().equals("администратор")) {
            modelAndView.addObject("lc", categoryDao.get(2));
            modelAndView.addObject("llcc", categoryDao.list(2));
        }

        modelAndView.addObject("userJSP", user);

        Product  product = new Product();

        product.setSerialNumber(0);
        product.setCategory(categoryDao.get(Integer.parseInt(request.getParameter("category"))));
        product.setProductName(request.getParameter("product_name"));
        product.setPrice(Double.parseDouble(request.getParameter("price")));
        product.setDescription(request.getParameter("discription"));
        product.setStatus(true);
        product.setDiscount(Integer.parseInt(request.getParameter("discount")));
        product.setManufacturer(user.getName());

        System.out.println(product.toString());
        productDao.addProduct(product);


        return modelAndView;
    }
}
