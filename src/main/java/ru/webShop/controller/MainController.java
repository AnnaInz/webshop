package ru.webShop.controller;

/**
 * Created by Анна on 28.04.2017.
 */
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import ru.webShop.dao.impl.CategoryDaoImpl;
import ru.webShop.dao.impl.OrderDaoImpl;
import ru.webShop.dao.impl.ProductDaoImpl;
import ru.webShop.models.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import ru.javastudy.springMVC.model.User;


@Controller
@SessionAttributes("userJSP")
public class MainController {

    @Autowired
    private CategoryDaoImpl categoryDao;

    @Autowired
    private ProductDaoImpl productDao;

    @Autowired
    private OrderDaoImpl orderDao;

    @ModelAttribute("userJSP")
    public User createUser() {
        return new User();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(HttpServletRequest request, HttpServletResponse response, SessionStatus sessionStatus, @ModelAttribute("userJSP") User user) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        modelAndView.addObject("lc1", categoryDao.get(1));
        modelAndView.addObject("lc2", categoryDao.get(2));

        modelAndView.addObject("userJSP", user);

        return modelAndView;
    }

    @RequestMapping(value = "/category/{category}", method = RequestMethod.GET)
    public ModelAndView getPageCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable("category")  String category, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);

        modelAndView.setViewName("category");

        modelAndView.addObject("lc1", categoryDao.get(1));
        modelAndView.addObject("lc2", categoryDao.get(2));

        modelAndView.addObject("productJSP", productDao.getByCategory(categoryDao.getId(category)));
        modelAndView.addObject("categoryJSP", category);

        return modelAndView;
    }

    @RequestMapping(value = "/category/{category}", method = RequestMethod.POST)
    public ModelAndView executePageCategory(HttpServletRequest request, HttpServletResponse response, @PathVariable("category") String category, @ModelAttribute("userJSP") User user, SessionStatus sessionStatus) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userJSP", user);
        modelAndView.setViewName("category");

        String goodId = request.getParameter("product_id");
        String qantaty = request.getParameter("quantatyJSP");
        orderDao.addToCart(user, productDao.getProductBySerialNumber(Integer.valueOf(goodId)), 1);

        modelAndView.addObject("lc1", categoryDao.get(1));
        modelAndView.addObject("lc2", categoryDao.get(2));

        modelAndView.addObject("productJSP", productDao.getByCategory(categoryDao.getId(category)));
        modelAndView.addObject("categoryJSP", category);

        return modelAndView;
    }

}