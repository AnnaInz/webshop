package ru.webShop.dao;

import ru.webShop.models.Pair;
import ru.webShop.models.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Анна on 04.05.2017.
 */
public interface ProductDao {
    public Product getProductByName(String productName); //выбрать/найти продукты по имени или серийному номеру
    public Product getProductBySerialNumber (long serialNumber);
    public List<Product> getByCategory(int categoryId);
    public List<Product> getByManufacturer(String manufacturer);
    public List<Pair<Product, Integer>> getByUser(int userId); //чтобы знать что у него в корзине

    public List<Product> getProductOnPrice(double price); //выбрать продукты по цене

    public String addProduct(Product product); //добавление и удаление продуктов из бд
    public String delete(long serialNumber);

    public boolean setPrice(long serialNumber, double price);
    public boolean setDiscount(long serialNumber, int discount);
    public boolean setStatus(long serialNumber, boolean status);

    public boolean existProduct(long serialNumber);
}
