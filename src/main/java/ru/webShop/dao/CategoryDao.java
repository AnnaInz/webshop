package ru.webShop.dao;

import ru.webShop.models.Category;

import java.util.List;

/**
 * Created by Анна on 04.05.2017.
 */
public interface CategoryDao {
    public String add(Category category);
    public String delete(Category category);

    public Category get(String name);
    public Category get(int id);

    List<Category> getByParent(int parent);
    public int getId(String name);

    boolean conteinsCategory(String name);

    public List<Category> list(int id_parent);
}
