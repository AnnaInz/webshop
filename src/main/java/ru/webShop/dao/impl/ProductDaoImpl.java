package ru.webShop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.webShop.dao.ProductDao;
import ru.webShop.models.Pair;
import ru.webShop.models.Product;
import java.util.List;

/**
 * Created by Анна on 04.05.2017.
 */

public class ProductDaoImpl implements ProductDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public Product getProductByName(String productName) {
        String sql = "SELECT * FROM product WHERE status = true AND product_name=" + productName;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Product product = new Product();

                CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
                categoryDAO.setJdbcTemplate(jdbcTemplate);

                product.setSerialNumber(rs.getLong("serial_number"));
                product.setCategory(categoryDAO.get(rs.getInt("id_category")));
                product.setProductName(rs.getString("product_name"));
                product.setPrice(rs.getDouble("price"));
                product.setDescription(rs.getString("discription"));
                product.setStatus(rs.getBoolean("status"));
                product.setDiscount(rs.getInt("discount"));
                product.setManufacturer(rs.getString("manufacturer"));

                String sqlImg = "SELECT * FROM pictures WHERE serial_number_of_product=" + product.getSerialNumber();
                List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("picture_link"));

                product.setPhotos(listImg);

                return product;
            }
            return null;
        });
    }

    @Override
    public Product getProductBySerialNumber(long serialNumber) {
        String sql = "SELECT * FROM product WHERE status = true AND serial_number=" + serialNumber;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Product product = new Product();

                CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
                categoryDAO.setJdbcTemplate(jdbcTemplate);

                product.setSerialNumber(rs.getLong("serial_number"));
                product.setCategory(categoryDAO.get(rs.getInt("id_category")));
                product.setProductName(rs.getString("product_name"));
                product.setPrice(rs.getDouble("price"));
                product.setDescription(rs.getString("discription"));
                product.setStatus(rs.getBoolean("status"));
                product.setDiscount(rs.getInt("discount"));
                product.setManufacturer(rs.getString("manufacturer"));

                String sqlImg = "SELECT * FROM pictures WHERE serial_number_of_product=" + product.getSerialNumber();
                List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("picture_link"));

                product.setPhotos(listImg);

                return product;
            }
            return null;
        });
    }

    @Override
    public List<Product> getByCategory(int categoryId) {
        String sql = "SELECT * FROM product WHERE status = true AND id_category =" + categoryId;
        List<Product> listProduct = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Product product = new Product();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            product.setSerialNumber(rs.getLong("serial_number"));
            product.setCategory(categoryDAO.get(rs.getInt("id_category")));
            product.setProductName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("price"));
            product.setDescription(rs.getString("discription"));
            product.setStatus(rs.getBoolean("status"));
            product.setDiscount(rs.getInt("discount"));
            product.setManufacturer(rs.getString("manufacturer"));

            String sqlImg = "SELECT * FROM pictures WHERE serial_number_of_product=" + product.getSerialNumber();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("picture_link"));

            product.setPhotos(listImg);

            return product;
        });

        return listProduct;
    }

    @Override
    public List<Product> getProductOnPrice(double price) {
        String sql = "SELECT * FROM product WHERE status = true AND price <=" + price;
        List<Product> listProduct = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Product product = new Product();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            product.setSerialNumber(rs.getLong("serial_number"));
            product.setCategory(categoryDAO.get(rs.getInt("id_category")));
            product.setProductName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("price"));
            product.setDescription(rs.getString("discription"));
            product.setStatus(rs.getBoolean("status"));
            product.setDiscount(rs.getInt("discount"));
            product.setManufacturer(rs.getString("manufacturer"));

            String sqlImg = "SELECT * FROM pictures WHERE serial_number_of_product=" + product.getSerialNumber();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("image_path"));

            product.setPhotos(listImg);

            return product;
        });

        return listProduct;
    }

    @Override
    public List<Product> getByManufacturer(String manufacturer) {
        String sql = "SELECT * FROM product WHERE status = true AND manufacturer='" + manufacturer + "'";
        List<Product> listProduct = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Product product = new Product();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            product.setSerialNumber(rs.getLong("serial_number"));
            product.setCategory(categoryDAO.get(rs.getInt("id_category")));
            product.setProductName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("price"));
            product.setDescription(rs.getString("discription"));
            product.setStatus(rs.getBoolean("status"));
            product.setDiscount(rs.getInt("discount"));
            product.setManufacturer(rs.getString("manufacturer"));

            String sqlImg = "SELECT * FROM pictures WHERE serial_number_of_product=" + product.getSerialNumber();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("picture_link"));

            product.setPhotos(listImg);

            return product;
        });

        return listProduct;
    }

    @Override
    public String addProduct(Product product) {
        boolean update = false;
        if (existProduct(product.getSerialNumber()))
            update = true;

        if (update) {
            String sql = "UPDATE product SET id_category=?, product_name=?, price=?, discription=?, status=?, discount=?, manufacturer=? WHERE serialnumber=?";
            jdbcTemplate.update(sql, product.getCategory().getId(),  product.getProductName(), product.getPrice(), product.getDescription(),
                    product.getStatus(), product.getDiscount(), product.getManufacturer(), product.getSerialNumber());
            return "update";
        } else {
            String sql = "INSERT INTO product (id_category, product_name, price, discription, status, discount, manufacturer)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, product.getCategory().getId(), product.getProductName(), product.getPrice(), product.getDescription(),
                    product.getStatus(), product.getDiscount(), product.getManufacturer());
            return "insert";
        }
    }

    @Override
    public String delete(long serialNumber) {
        boolean delete = false;
        if (existProduct(serialNumber))
            delete = true;

        if (delete) {
            String sql = "DELETE FROM product WHERE serial_number=?";
            jdbcTemplate.update(sql, serialNumber);
            return "delete";
        }

        return "no product";
    }

    @Override
    public boolean setPrice(long serialNumber, double price) {
        if (!existProduct(serialNumber))
            return false;

        String sql = "UPDATE product SET price=? WHERE serial_number=?";
        jdbcTemplate.update(sql, price, serialNumber);

        return true;
    }

    @Override
    public boolean setDiscount(long serialNumber, int discount) {
        if (!existProduct(serialNumber))
            return false;

        String sql = "UPDATE product SET discount=? WHERE serial_number=?";
        jdbcTemplate.update(sql, discount, serialNumber);

        return true;
    }

    @Override
    public boolean setStatus(long serialNumber, boolean status) {
        if (!existProduct(serialNumber))
            return false;

        String sql = "UPDATE product SET status=? WHERE serial_number=?";
        jdbcTemplate.update(sql, status, serialNumber);

        return true;
    }

    @Override
    public List<Pair<Product, Integer>> getByUser(int userId) {
        String sql = "SELECT list_of_order.count, product.status, orders.id_of_user, product.serial_number, product.id_category, product.product_name, product.price, product.discription, product.discount, product.manufacturer FROM product JOIN list_of_order ON (list_of_order.serial_product = product.serial_number) JOIN orders ON (list_of_order.order_id = orders.id_of_order) WHERE orders.status = 'В корзине' AND orders.id_of_user = " + userId;
        List<Pair<Product, Integer>> listProduct = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Product product = new Product();

            CategoryDaoImpl categoryDAO = new CategoryDaoImpl();
            categoryDAO.setJdbcTemplate(jdbcTemplate);

            product.setSerialNumber(rs.getLong("serial_number"));
            product.setCategory(categoryDAO.get(rs.getInt("id_category")));
            product.setProductName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("price"));
            product.setDescription(rs.getString("discription"));
            product.setStatus(rs.getBoolean("status"));
            product.setDiscount(rs.getInt("discount"));
            product.setManufacturer(rs.getString("manufacturer"));

            String sqlImg = "SELECT * FROM pictures WHERE serial_number_of_product=" + product.getSerialNumber();
            List<String> listImg = jdbcTemplate.query(sqlImg, (rs1, rowNum1) -> rs1.getString("picture_link"));

            product.setPhotos(listImg);

            int count = rs.getInt("count");

            return Pair.createPair(product, count);
        });

        return listProduct;
    }


    @Override
    public boolean existProduct(long serialNumber) {
        String sql = "SELECT * FROM product WHERE serial_number = " + serialNumber;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return true;
            }
            return  false;
        });
    }
}
