package ru.webShop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.webShop.dao.OrderDao;
import ru.webShop.models.Order;
import ru.webShop.models.Pair;
import ru.webShop.models.Product;
import ru.webShop.models.User;
import java.util.List;

/**
 * Created by Анна on 04.05.2017.
 */
public class OrderDaoImpl implements OrderDao {
    private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public List<Order> getByUserAndStatus(User user, String status) {
        String sql = "SELECT * FROM orders WHERE id_of_user=" + user.getID() + " AND status='" + status + "'";
        List<Order> lo = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Order order = new Order();
            order.setID(rs.getInt("id_of_order"));

            UserDaoImpl userDAO = new UserDaoImpl();
            userDAO.setJdbcTemplate(jdbcTemplate);

            order.setUser(userDAO.getByID(rs.getLong("id_of_user")));

            ProductDaoImpl productDAO = new ProductDaoImpl();
            productDAO.setJdbcTemplate(jdbcTemplate);

            String sqlProducts = "SELECT * FROM list_of_order WHERE order_id=" + order.getID();
            List<Pair<Product, Integer>> productList = jdbcTemplate.query(sqlProducts, (rsProduct, rowNumpProduct) -> {

                Product product = productDAO.getProductBySerialNumber(rsProduct.getLong("serial_product"));
                int count = rsProduct.getInt("count");

                return Pair.createPair(product, count);
            });
            order.setList(productList);

            order.setPrice(rs.getDouble("price"));
            order.setStatus(rs.getString("status"));

            return order;
        });

        return lo;
    }

    @Override
    public List<Order> getByUser(User user) {
        String sql = "SELECT * FROM orders WHERE id_of_user=" + user.getID();
        List<Order> lo = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Order order = new Order();
            order.setID(rs.getInt("id_of_order"));

            UserDaoImpl userDAO = new UserDaoImpl();
            userDAO.setJdbcTemplate(jdbcTemplate);
            order.setUser(userDAO.getByID(rs.getInt("id_of_user")));

            ProductDaoImpl productDAO = new ProductDaoImpl();
            productDAO.setJdbcTemplate(jdbcTemplate);
            String sqlProduct = "SELECT * FROM list_of_order WHERE order_id=" + order.getID();
            List<Pair<Product, Integer>> productList = jdbcTemplate.query(sqlProduct, (rsProduct, rowNumProduct) -> {
                Product product = productDAO.getProductBySerialNumber(rsProduct.getLong("serial_product"));
                int count = rsProduct.getInt("count");

                return Pair.createPair(product, count);
            });
            order.setList(productList);

            order.setPrice(rs.getDouble("price"));
            order.setStatus(rs.getString("status"));

            return order;
        });

        return lo;
    }

    @Override
    public String addOrder(Order order) {
        boolean update = false;
        if (order.getID() > 0)
            update = true;

        if (update) {
            // update
            String sql = "UPDATE orders SET id_of_user=?, price = ?, status=? WHERE id_of_order=?";
            jdbcTemplate.update(sql, order.getUser().getID(), order.getPrice(), order.getStatus(), order.getID());
            return "update";
        } else {
            // insert
            String sql = "INSERT INTO orders (id_of_user, price, status)"
                    + " VALUES (?, ?, ?)";
            jdbcTemplate.update(sql, order.getUser().getID(), order.getPrice(), order.getStatus());
            return "insert";
        }
    }

    @Override
    public String deleteOrder(long ID) {
        boolean delete = false;
        if (existOrder(ID))
            delete = true;

        if (delete) {
            String sql1 = "DELETE FROM list_of_order WHERE order_id=?";
            jdbcTemplate.update(sql1, ID);

            String sql = "DELETE FROM orders WHERE id_of_order=?";
            jdbcTemplate.update(sql, ID);

            return "delete";
        }

        return "no product";
    }

    @Override
    public void addToCart(User user, Product product, int count) {
        //если пользователь первую вещь кладет в корзину
        System.out.println(getByUserAndStatus(user, "В корзине"));
        System.out.println(getByUserAndStatus(user, "В корзине").isEmpty());

        if (getByUserAndStatus(user, "В корзине").isEmpty()) {
            Order order = new Order();
            order.setID(0);
            order.setStatus("В корзине");
            order.setUser(user);
            order.setPrice(0.0);

            addOrder(order);
        }

        String sql = "SELECT id_of_order FROM orders WHERE status = 'В корзине' AND id_of_user = " + user.getID();
        int orderId = jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        });

        if (!existProductInOrder(product.getSerialNumber(), orderId) && product.getStatus()) {
            sql = "INSERT INTO list_of_order(serial_product, order_id, \"count\", price_with_disc)"
                    + " VALUES (?, ?, ?, ?)";
            jdbcTemplate.update(sql, product.getSerialNumber(), orderId, 1, product.getPrice());

        }
    }

    @Override
    public void deteteFromCart(User user, Product product, int count) {
        String sql = "SELECT id_of_order FROM orders WHERE status = 'В корзине' AND id_of_user = " + user.getID();
        int orderId = jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        });

        sql = "DELETE FROM list_of_order WHERE order_id = ? AND serial_product = ?";
        jdbcTemplate.update(sql, orderId, product.getSerialNumber());


    }

    @Override
    public boolean existOrder(long id) {
        String sql = "SELECT count(1) FROM orders WHERE id_of_order = " + id;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }

    @Override
    public boolean existProductInOrder(long productId, long orderId) {
        String sql = "SELECT count(1) FROM list_of_order WHERE serial_product = ? AND order_id = ?;";
        return jdbcTemplate.query(sql, new Object[]{productId, orderId}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }

    @Override
    public void ordering(User user, double price) {
        String sql = "SELECT id_of_order FROM orders WHERE status = 'В корзине' AND id_of_user = " + user.getID();
        int orderId = jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt(1);
            }
            return 0;
        });

        Order order = new Order();
        order.setID(orderId);
        order.setStatus("Заказ оформляется");
        order.setUser(user);
        order.setPrice(price);

        addOrder(order);

        ProductDaoImpl productDAO = new ProductDaoImpl();
        productDAO.setJdbcTemplate(jdbcTemplate);

        String sqlProduct = "SELECT serial_product FROM list_of_order WHERE order_id=" + orderId;
        List<Long> productList = jdbcTemplate.query(sqlProduct, (rsProduct, rowNumProduct) -> {
            return rsProduct.getLong("serial_product");
        });

        for (Long idProduct : productList){
            productDAO.setStatus(idProduct, false);
        }

    }
}
