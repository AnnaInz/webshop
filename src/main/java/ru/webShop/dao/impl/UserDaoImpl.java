package ru.webShop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.webShop.dao.UserDao;
import ru.webShop.models.User;

import java.sql.SQLException;

/**
 * Created by Анна on 04.05.2017.
 */
@Repository
public class UserDaoImpl implements UserDao{
    private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public User getUserByEmail(String email) {
        String sql = "SELECT * FROM users WHERE \"email\"='" + email + "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                User user = new User();

                user.setID(rs.getInt("id"));
                user.setSurname(rs.getString("surname"));
                user.setName(rs.getString("name"));
                user.setSecondName(rs.getString("second_name"));
                user.setAddress(rs.getString("address"));
                user.setEmail(rs.getString("email"));
                user.setStatus(rs.getString("status"));
                user.setPassword(rs.getString("password"));

                return user;
            }
            return  null;
        });
    }

    @Override
    public User getByID(long id) {
        String sql = "SELECT * FROM users WHERE id=" + id;
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                User user = new User();

                user.setID(rs.getInt("id"));
                user.setSurname(rs.getString("surname"));
                user.setName(rs.getString("name"));
                user.setSecondName(rs.getString("second_name"));
                user.setAddress(rs.getString("address"));
                user.setEmail(rs.getString("email"));
                user.setStatus(rs.getString("status"));
                user.setPassword(rs.getString("password"));

                return user;
            }
            return  null;
        });
    }

    @Override
    public String add(User user) throws SQLException {
        boolean update = false;
        if (existUser(user.getEmail()))
            update = true;

        if (update) {
            String sql = "UPDATE users SET surname=?, \"name\"=?, second_name=?, address=?, email=?, password=? WHERE id=?";
            jdbcTemplate.update(sql, user.getSurname(), user.getName(), user.getSecondName(), user.getAddress(),
                    user.getEmail(), user.getPassword(), user.getID());
            return "update";
        } else {
            String sql = "INSERT INTO users (surname, \"name\", second_name, address, email, status, password)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?)";
            jdbcTemplate.update(sql, user.getSurname(), user.getName(), user.getSecondName(), user.getAddress(),
                    user.getEmail(), user.getStatus(), user.getPassword());
            System.out.println("Я здесь 3");
            return "insert";
        }
    }

    @Override
    public String remove(User user) throws SQLException {
        boolean delete = false;
        if (existUser(user.getEmail()))
            delete = true;

        if (delete) {
            String sql = "DELETE FROM users WHERE id=?";
            jdbcTemplate.update(sql, user.getID());
            return "delete";
        }

        return "no user";
    }

   /* @Override
    public boolean existUser(String email) throws SQLException {
        String sql = "SELECT count(1) FROM users WHERE email = ? AND password = ?";
        return jdbcTemplate.query(sql, new Object[]{email, password}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
        String sql = "SELECT count(1) FROM users WHERE email = ?";
        return jdbcTemplate.query(sql, new Object[]{email }, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return false;
        });
    }*/

    @Override
    public boolean isValidUser(String email, String password) throws SQLException {
        String sql = "SELECT * FROM users WHERE email = ? AND password = ?";
        return jdbcTemplate.query(sql, new Object[]{email, password}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }

    @Override
    public boolean existUser(String email) throws SQLException {
        String sql = "SELECT count(1) FROM users WHERE email = ?";
        return jdbcTemplate.query(sql, new Object[]{email}, rs -> {
            if (rs.next()) {
                return (rs.getInt(1) > 0);
            }
            return  false;
        });
    }
}
