package ru.webShop.dao.impl;

import org.springframework.jdbc.core.JdbcTemplate;
import ru.webShop.dao.CategoryDao;
import ru.webShop.models.Category;
import ru.webShop.models.Product;

import java.util.List;

/**
 * Created by Анна on 04.05.2017.
 */
public class CategoryDaoImpl implements CategoryDao {
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public String add(Category category) {
        boolean update = false;
        if (conteinsCategory(category.getName()))
            update = true;

        if (update) {
            String sql = "UPDATE category SET name=?, id_parent=? WHERE id=?";
            jdbcTemplate.update(sql, category.getName(), category.getId());
            return "update";
        } else {
            String sql = "INSERT INTO category (name, id_parent)"
                    + " VALUES (?, ?)";
            jdbcTemplate.update(sql,  category.getName(), category.getId());
            return "insert";
        }
    }

    @Override
    public String delete(Category category) {
        boolean delete = false;
        if (conteinsCategory(category.getName()))
            delete = true;

        if (delete) {
            ProductDaoImpl productDao = new ProductDaoImpl();
            productDao.setJdbcTemplate(jdbcTemplate);

            List<Product> del = productDao.getByCategory(category.getId());
            for (Product product : del)
                productDao.delete(product.getSerialNumber());


            String sql = "DELETE FROM category WHERE id=?";
            jdbcTemplate.update(sql, category.getId());
            return "delete";
        }

        return "no category";
    }

    @Override
    public Category get(String name) {
        String sql = "SELECT * FROM category WHERE name='" + name + "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Category category = new Category();

                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
                category.setParentCategory(rs.getInt("id_parent"));

                category.setSubcategory(getByParent(category.getId()));
                return category;
            }

            return null;
        });
    }

    @Override
    public Category get(int id) {
        String sql = "SELECT * FROM category WHERE id='" + id+ "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                Category category = new Category();

                category.setId(rs.getInt("id"));
                category.setName(rs.getString("name"));
                category.setParentCategory(rs.getInt("id_parent"));

                category.setSubcategory(getByParent(category.getId()));
                return category;
            }

            return null;
        });
    }

    @Override
    public int getId(String name) {
        String sql = "SELECT * FROM category WHERE name='" + name + "'";
        return jdbcTemplate.query(sql, rs -> {
            if (rs.next()) {
                return rs.getInt("id");
            }
            return null;
        });
    }

    @Override
    public List<Category> getByParent(int parent) {
        String sql = "SELECT * FROM category WHERE id_parent=" + parent;
        List<Category> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Category category = new Category();
            category.setId(rs.getInt("id"));
            category.setName(rs.getString("name"));
            category.setParentCategory(rs.getInt("id_parent"));

            category.setSubcategory(getByParent(category.getId()));

            return category;
        });

        return listCategories;
    }

    @Override
    public boolean conteinsCategory(String name) {
        String sql = "SELECT * FROM category WHERE name = ?";
        return jdbcTemplate.query(sql, new Object[]{name}, rs -> {
            if (rs.next()) {
                return true;
            }
            return  false;
        });
    }



    @Override
    public List<Category> list(int id_parent) {
        String sql = "SELECT * FROM category WHERE id_parent = " + id_parent ;
        List<Category> listCategories = jdbcTemplate.query(sql, (rs, rowNum) -> {
            Category category = new Category();
            category.setId(rs.getInt("id"));
            category.setName(rs.getString("name"));
            category.setParentCategory(rs.getInt("id_parent"));

            category.setSubcategory(getByParent(category.getId()));

            return category;
        });

        return listCategories;
    }
}
