package ru.webShop.dao;

import ru.webShop.models.User;

import java.sql.SQLException;

/**
 * Created by Анна on 04.05.2017.
 */
public interface UserDao {
    public User getUserByEmail(String email); //найти пользвателя по email, который в качестве логина
    public User getByID(long id);//найти пользователя по id
    public String add(User user) throws SQLException; //добавить или обновить в бд
    public String remove(User user) throws SQLException;//удалить из бд
    boolean existUser(String username) throws SQLException; //есть ли такой юзер
    boolean isValidUser(String email, String password) throws SQLException; //для логина
}
