package ru.webShop.dao;

import ru.webShop.models.Order;
import ru.webShop.models.Product;
import ru.webShop.models.User;

import java.util.List;

/**
 * Created by Анна on 04.05.2017.
 */
public interface OrderDao {
    List<Order> getByUserAndStatus(User user, String status);
    List<Order> getByUser(User user);

    public String addOrder(Order order);
    public String deleteOrder(long ID);

    public void addToCart(User user, Product product, int count);
    public void deteteFromCart(User user, Product product, int count);

    public boolean existOrder(long id);
    public boolean existProductInOrder(long productId, long orderId);

    void ordering(User user, double price);

}
